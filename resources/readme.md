#Resources

## Teaching Computational Physics to Undergraduates
The pdf can be found [here](https://gitlab.com/computational_physics/homework/blob/master/resources/teaching_comp_phy_to_under_grads.pdf). A summary
of benefits of teaching comp phys with an interesting view on differences between
physics and other areas.